// copyToClipboard method puts the content passed to this method to the clipboard of the
// host system.
function copyToClipboard(texttocopy){
         var dummy = $('<textarea>').val(texttocopy).appendTo('body').select();
         document.execCommand('copy');
         $(dummy).remove();
        }
